//
//  BadTinderApp.swift
//  BadTinder
//
//  Created by smarthaggarwal on 12/09/22.
//

import SwiftUI

@main
struct BadTinderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
