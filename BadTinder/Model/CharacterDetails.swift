//
//  CharacterDetails.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import Foundation
import SwiftUI
struct CharacterDetails: Hashable, Codable {
    var char_id: Int
    var name: String
    var birthday: String
    var occupation: [String]
    var img: String
    var status: String
    var nickname: String
    var appearance:[Int]
    var portrayed: String
    var category:String
    var better_call_saul_appearance:[Int]
    var image: Image {
        Image(img)
    }
}

extension CharacterDetails: Identifiable {
  var id: Int { char_id }
}
