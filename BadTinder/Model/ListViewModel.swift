//
//  ListViewModel.swift
//  BreakingBad
//
//  Created by smarthaggarwal on 08/09/22.
//

import Foundation

class ListViewModel{
    let networkManager = HTTPNetworking()
    func getCharacters(completion:@escaping([CharacterDetails]?)->Void){
        networkManager.request(fromPath: "characters") { data, error in
            if let err = error{
                completion(nil)
            }
            let decoded = self.decodeJSON(type:[CharacterDetails].self,from:data)
            if let decoded = decoded{
            }
            completion(decoded)
        }
    }
    
    private func decodeJSON<T:Decodable>(type:T.Type,from:Data?)->T?{
        let decoder = JSONDecoder()
        guard let data = from, let response = try? decoder.decode(type.self, from: data) else{
            return nil
        }
        return response
    }
}
