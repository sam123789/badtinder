//
//  UserInfoView.swift
//  BadTinder
//
//  Created by smarthaggarwal on 12/09/22.
//

import SwiftUI

struct UserInfoView: View {
    var characterName:String
    var occupation:String
    var body: some View {
        VStack(alignment: .leading) {
            Spacer()
            Text(characterName)
                .font(.title)
                .fontWeight(.regular)
                .foregroundColor(.white)
            Text(occupation)
                .font(.system(size: 17))
                .fontWeight(.light)
                .foregroundColor(.white)
        }
        .padding()
    }
}

//struct UserInfoView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserInfoView()
//    }
//}
