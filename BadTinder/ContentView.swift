//
//  ContentView.swift
//  BadTinder
//
//  Created by smarthaggarwal on 12/09/22.
//

import SwiftUI

struct ContentView : View {
    @State var characters = [CharacterDetails]()
    var body: some View {
        VStack(alignment: .center, spacing: 20) {
            ZStack {
                ForEach(characters) { person in
                    CardView(character:person)
                        .padding(.horizontal, 10)
                }
            }
        }
        .padding(.bottom, 15)
        .onAppear(perform: loadData)
    }
    
    func loadData(){
        ListViewModel().getCharacters(completion: { charDetails in
            if charDetails != nil{
                characters = charDetails!.shuffled().reversed()
            }
        })
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        CardView()
    }
}
#endif
