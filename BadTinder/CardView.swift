//
//  CardView.swift
//  BadTinder
//
//  Created by smarthaggarwal on 12/09/22.
//

import SwiftUI

struct CardView: View {
    @State private var offset = CGSize.zero
    var character:CharacterDetails?
    var body: some View {
        ZStack(alignment: .leading) {
            UserImageView(characterImage: character?.img)
            UserInfoView(characterName: character?.name ?? "Unknown", occupation: character?.occupation.first ?? "Unknown")
        }
        .shadow(radius: 2)
        .offset(x: offset.width * 1, y: offset.height * 0.4)
        .rotationEffect(.degrees(Double(offset.width / 40)))
        .gesture(
            DragGesture()
                .onChanged { gesture in
                    offset = gesture.translation
                }
                .onEnded { _ in
                    withAnimation {
                        swipeCard(width: offset.width)
                    }
                }
        )
    }
    
    func swipeCard(width: CGFloat) {
        switch width {
        case -500...(-150):
            offset = CGSize(width: -500, height: 0)
        case 150...500:
            offset = CGSize(width: 500, height: 0)
        default:
            offset = .zero
        }
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView()
    }
}
