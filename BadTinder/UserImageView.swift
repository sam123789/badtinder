//
//  UserImageView.swift
//  BadTinder
//
//  Created by smarthaggarwal on 12/09/22.
//

import SwiftUI

struct UserImageView: View {
    var characterImage:String?
    var body: some View {
        CacheAsyncImage(url: URL(string: characterImage!)!, content: { phase in
            switch phase {
            case .empty:
                ZStack{
                    Rectangle().fill (
                        LinearGradient(gradient: Gradient(colors: [ .black]),
                                       startPoint: .center, endPoint: .bottom)
                    )
                    .clipped()
                ProgressView()
                    
                }
            case .success(let image):
                image.resizable()
                    .overlay (
                        Rectangle()
                            .fill (
                                LinearGradient(gradient: Gradient(colors: [ .clear,.black]),
                                               startPoint: .center, endPoint: .bottom)
                            )
                            .clipped()
                    )
                    .cornerRadius(10, antialiased: true)
            case .failure:
//                Image(systemName: "photo")
                Image("defaultImage").resizable()
                    .overlay (
                        Rectangle()
                            .fill (
                                LinearGradient(gradient: Gradient(colors: [ .clear,.black]),
                                               startPoint: .center, endPoint: .bottom)
                            )
                            .clipped()
                    )
                    .cornerRadius(10, antialiased: true)
            @unknown default:
                // Since the AsyncImagePhase enum isn't frozen,
                // we need to add this currently unused fallback
                // to handle any new cases that might be added
                // in the future:
                EmptyView().overlay (
                    Rectangle()
                        .fill (
                            LinearGradient(gradient: Gradient(colors: [ .black]),
                                           startPoint: .center, endPoint: .bottom)
                        )
                        .clipped()
                )
                    .cornerRadius(10, antialiased: true)
            }
        })
    }
}

//struct UserImageView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserImageView()
//    }
//}
